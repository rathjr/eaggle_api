package com.example.eaggleapi.repository;

import com.example.eaggleapi.model.Player;
import com.example.eaggleapi.utils.Paging;
import com.twilio.twiml.Play;
import lombok.Setter;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Set;
@Mapper
public interface PlayerRepository {

    @Select("select * from players")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "placeOfBirth", column = "place_of_birth"),
            @Result(property = "bestFoot", column = "best_foot"),
            @Result(property = "createDate", column = "created_date")
    })
    List<Player> getAllPlayers();

    @Insert("INSERT INTO players (name,number,position,place_of_birth,height,weight,jointed,best_foot,size,images) VALUES (#{name}, #{number},#{position}, #{placeOfBirth},#{height},#{weight},#{jointed},#{bestFoot},#{size},#{images})")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    boolean create(Player player);

    @Select("SELECT * FROM players where id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "placeOfBirth", column = "place_of_birth"),
            @Result(property = "bestFoot", column = "best_foot"),
            @Result(property = "createDate", column = "created_date")
    })
    Player findPlayerByID(Long id);

    @Delete("delete from players where id=#{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "placeOfBirth", column = "place_of_birth"),
            @Result(property = "bestFoot", column = "best_foot"),
            @Result(property = "createDate", column = "created_date")
    })
    boolean deletePlayer(Long id);

    @Update("UPDATE players SET name = #{name},number=#{number},position=#{position},place_of_birth=#{placeOfBirth},height=#{height},weight=#{weight},jointed=#{jointed},best_foot=#{bestFoot},size=#{size},images=#{images} WHERE id = #{id}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "placeOfBirth", column = "place_of_birth"),
            @Result(property = "bestFoot", column = "best_foot"),
            @Result(property = "createDate", column = "created_date")
    })
    boolean updatePlayer(Player player);


    @Select("SELECT * FROM players where position=#{ps}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "placeOfBirth", column = "place_of_birth"),
            @Result(property = "bestFoot", column = "best_foot"),
            @Result(property = "createDate", column = "created_date")
    })
    List<Player> findAllPlayerbyPosition(String ps);



    @Select("SELECT * from players limit #{limit} offset #{offset}")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "placeOfBirth", column = "place_of_birth"),
            @Result(property = "bestFoot", column = "best_foot"),
            @Result(property = "createDate", column = "created_date")
    })
    List<Player> findByPaging(Paging paging);

    @Select("select count(id) from players")
    int countAllReport();
}
