package com.example.eaggleapi.repository;

import com.example.eaggleapi.model.auth.ERole;
import com.example.eaggleapi.model.auth.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.Optional;

@Mapper
public interface RoleRepository {
    @Select("SELECT * FROM roles WHERE name = #{name}")
    Optional<Role> findByName(ERole role);
}
