package com.example.eaggleapi.repository;

import com.example.eaggleapi.model.Contact;
import com.example.eaggleapi.model.Player;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ContactRepository {
    @Select("select * from contact")
    @Results({
            @Result(property = "phoneNumber", column = "phone_number"),
            @Result(property = "createDate", column = "created_date")
    })
    List<Contact> getAllContact();

    @Select("SELECT * FROM contact where id=#{id}")
    @Results({
            @Result(property = "phoneNumber", column = "phone_number"),
            @Result(property = "createDate", column = "created_date")
    })
    Contact findContactByID(Long id);

    @Insert("INSERT INTO contact (name,email,phone_number) VALUES (#{name}, #{email}, #{phoneNumber})")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    boolean create(Contact contact);

    @Delete("delete from contact where id=#{id}")
    @Results({
            @Result(property = "phoneNumber", column = "phone_number"),
            @Result(property = "createDate", column = "created_date")
    })
    boolean deleteContact(Long id);

}
