package com.example.eaggleapi.repository;

import com.example.eaggleapi.model.Match;
import com.example.eaggleapi.model.Player;
import com.example.eaggleapi.utils.Paging;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface MatchRepository {
    @Select("select * from matchs")
    @Results({
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "dateOfMatch", column = "date_of_match")
    })

    List<Match> getAllMatchs();

    @Insert("INSERT INTO matchs (name,logo,result,stadium,date_of_match,ename,elogo,time,type,lresult) VALUES (#{name}, #{logo}, #{result},#{stadium},#{dateOfMatch},#{ename},#{elogo},#{time},#{type},#{lresult})")
    @Options(keyColumn = "id", keyProperty = "id", useGeneratedKeys = true)
    boolean create(Match match);

    @Select("SELECT * FROM matchs where id=#{id}")
    @Results({
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "dateOfMatch", column = "date_of_match")
    })
    Match findMatchByID(Long id);

    @Delete("delete from matchs where id=#{id}")
    @Results({
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "dateOfMatch", column = "date_of_match")
    })
    boolean deleteMatch(Long id);

    @Update("UPDATE matchs SET name = #{name},logo=#{logo},result=#{result},stadium=#{stadium},date_of_match=#{dateOfMatch},ename=#{ename},elogo=#{elogo},time=#{time},type=#{type},lresult=#{lresult} WHERE id = #{id}")
    @Results({
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "dateOfMatch", column = "date_of_match")
    })
    boolean updateMatch(Match match);

    @Select("SELECT * from matchs limit #{limit} offset #{offset}")
    @Results({
            @Result(property = "createDate", column = "created_date"),
            @Result(property = "dateOfMatch", column = "date_of_match")
    })
    List<Match> findByPaging(Paging paging);

    @Select("select count(id) from matchs")
    int countAllMatch();
}
