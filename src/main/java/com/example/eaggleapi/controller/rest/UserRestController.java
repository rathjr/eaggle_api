package com.example.eaggleapi.controller.rest;

import com.example.eaggleapi.model.auth.User;
import com.example.eaggleapi.payload.ResponseEntity;
import com.example.eaggleapi.payload.request.UserRequest;
import com.example.eaggleapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserService userService;

    @GetMapping
    public ResponseEntity<List<User>> findAllUsers() {
        try {
//             declare list for store all user then return result
            List<User> user = userService.findAllUser();
            return ResponseEntity
                    .<List<User>>ok()
                    .setData(user).setMessage("User have been found successfully");
        } catch (Exception ex) {
            return ResponseEntity
                    .<List<User>>exception()
                    .setMessage(ex.getMessage());
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<User> updateUser(@RequestBody UserRequest userRequest, @PathVariable Long id) {
//        find user by id first if found then process update
        User user = new User();
        user = userService.findUserByID(id);
        if (user == null) {
            return ResponseEntity
                    .<User>badRequest()
                    .setMessage("Your ID " + id + " is Not Found !");
        } else {
//        set data for update to post
            user.setName(userRequest.getName());
            user.setPhoneNumber(userRequest.getPhoneNumber());
            user.setProfile(userRequest.getProfile());
            boolean isUpdate = userService.updateUser(user);
            if (isUpdate) {
                return ResponseEntity.<User>ok().setData(user).setMessage("User ID " + id + " have been updated successfully");
            } else
                return ResponseEntity.<User>badRequest().setMessage("Check your Data again");
        }
    }

    @GetMapping("/view/{id}")
    public ResponseEntity<User> findUserByID(@PathVariable Long id) {
//       declare object for call method findByID if found it will return successfully
        User user = userService.findUserByID(id);
        if (user == null) {
            return ResponseEntity
                    .<User>badRequest()
                    .setMessage("Your ID " + id + " is Not Found !");
        } else {
            return ResponseEntity
                    .<User>ok()
                    .setData(user).setMessage("User ID " + id + " have been found successfully");
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<User> delete(@PathVariable Long id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            return ResponseEntity.<User>badRequest().setMessage("You cannot update this post. Please login first.");
        } else {
            //       find users by ID first before delete
            User user = userService.findUserByID(id);
            if (user == null) {
                return ResponseEntity
                        .<User>badRequest()
                        .setMessage("Your ID " + id + " is Not Found !");
            } else {
//            if found then it will delete
                boolean isDel = userService.deleteUsers(id);
                if (isDel) {
                    return ResponseEntity.<User>ok().setData(user).setMessage("User ID " + id + " have been delete successfully");
                } else
                    return ResponseEntity.<User>badRequest().setMessage("Check your Data again");
            }
        }
    }
}
