package com.example.eaggleapi.controller.rest;

import com.example.eaggleapi.model.Match;
import com.example.eaggleapi.model.Player;
import com.example.eaggleapi.payload.ResponseEntity;
import com.example.eaggleapi.payload.request.MatchRequest;
import com.example.eaggleapi.payload.request.PlayerRequest;
import com.example.eaggleapi.service.MatchService;
import com.example.eaggleapi.service.PlayerService;
import com.example.eaggleapi.utils.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/match")
public class MatchRestController {
    @Autowired
    MatchService matchService;

    @GetMapping
    public ResponseEntity<List<Match>> findAllMatch() {
        try {
//            declare list for store all post then return result
            List<Match> mtc = matchService.getAllMatchs();
            return ResponseEntity
                    .<List<Match>>ok()
                    .setData(mtc).setMessage("Match have been found successfully");
        } catch (Exception ex) {
            return ResponseEntity
                    .<List<Match>>exception()
                    .setMessage(ex.getMessage());
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Match> createPost(@RequestBody MatchRequest matchRequest) {
        Match match = new Match();
       match.setName(matchRequest.getName());
       match.setResult(matchRequest.getResult());
       match.setStadium(matchRequest.getStadium());
       match.setLogo(matchRequest.getLogo());
       match.setElogo(matchRequest.getElogo());
       match.setEname(matchRequest.getEname());
       match.setTime(matchRequest.getTime());
       match.setType(matchRequest.getType());
       match.setLresult(matchRequest.getLresult());
       match.setDateOfMatch(matchRequest.getDateOfMatch());
//        declare boolean for call method create if true post well insert success
        boolean isCreate = matchService.create(match);
        if (isCreate)
            return ResponseEntity.<Match>ok().setData(match).setMessage("Match have been create successfully");
        else
            return ResponseEntity.<Match>badRequest().setMessage("Check your Data again");
    }

    @GetMapping("/{id}/view")
    public ResponseEntity<Match> findPlayerById(@PathVariable long id) {
//      declare object for call method findByID if found it will return successfully
        Match match = matchService.findMatchByID(id);
        if (match == null) {
            return ResponseEntity
                    .<Match>badRequest()
                    .setMessage("Your ID " + id + " is Not Found !");
        } else {
            return ResponseEntity
                    .<Match>ok()
                    .setData(match).setMessage("Match ID " + id + " have been found successfully");
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Match> updatePost(@PathVariable Long id,
                                             @RequestBody MatchRequest matchRequest) {
//        find post by id first if found then process update
        Match match = matchService.findMatchByID(id);
        if (match == null) {
            return ResponseEntity
                    .<Match>badRequest()
                    .setMessage("Your ID " + id + " is Not Found !");
//         set data for update to post
        } else {
            match.setName(matchRequest.getName());
            match.setResult(matchRequest.getResult());
            match.setStadium(matchRequest.getStadium());
            match.setLogo(matchRequest.getLogo());
            match.setElogo(matchRequest.getElogo());
            match.setEname(matchRequest.getEname());
            match.setTime(matchRequest.getTime());
            match.setType(matchRequest.getType());
            match.setLresult(matchRequest.getLresult());
            match.setDateOfMatch(matchRequest.getDateOfMatch());
            boolean isUpdate = matchService.updateMatch(match);
            if (isUpdate) {
                return ResponseEntity.<Match>ok().setData(match).setMessage("Match ID " + id + " have been updated successfully");
            } else
                return ResponseEntity.<Match>badRequest().setMessage("Check your Data again");
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Match> delete(@PathVariable Long id) {
//       find post by ID first before delete
        Match match = matchService.findMatchByID(id);
        if (match== null) {
            return ResponseEntity
                    .<Match>badRequest()
                    .setMessage("Your ID " + id + " is Not Found !");
        } else {
//            if found then it will delete
            boolean isDel = matchService.deleteMatch(id);
            if (isDel) {
                return ResponseEntity.<Match>ok().setData(match).setMessage("Match ID" + id + " have been delete successfully");
            } else
                return ResponseEntity.<Match>badRequest().setMessage("Check your Data again");
        }
    }
    @GetMapping("/paging")
    public ResponseEntity<List<Match>> findByPaging(@RequestParam int page, @RequestParam int limit) {
//        declare var as int for call method count
        int countAllMatch = matchService.countAllMatch();
        Paging paging = new Paging();
//        set page limit TotalCount to paging then return result
        paging.setPage(page);
        paging.setLimit(limit);
        paging.setTotalCount(countAllMatch);
        List<Match> matchs = matchService.findByPaging(paging);
        return ResponseEntity
                .<List<Match>>ok()
                .setData(matchs).setMetadata(paging);
    }
}

