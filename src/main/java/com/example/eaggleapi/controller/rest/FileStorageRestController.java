package com.example.eaggleapi.controller.rest;

import com.example.eaggleapi.model.FileStorage;
import com.example.eaggleapi.payload.ResponseEntity;
import com.example.eaggleapi.service.Imp.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/files")
public class FileStorageRestController {
    private FileStorageService storageService;

    @Value("${file.upload.full.url}")
    private String fullURL;

    @Autowired
    public void setStorageService(FileStorageService storageService) {
        this.storageService = storageService;
    }

    @RequestMapping(value = "/upload",method = RequestMethod.POST,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<FileStorage> uploadFile(@RequestParam("file") MultipartFile file) {
        FileStorage fileStorage=new FileStorage();
        fileStorage.setUrl(fullURL + storageService.storeFile(file));
        return ResponseEntity.<FileStorage>ok().setData(fileStorage).setMessage("Images have been upload successfully ");
//        return fullURL + storageService.storeFile(file);
    }

    @DeleteMapping("/cleardirectory")
    public ResponseEntity<FileStorage> deleteAll() {
        this.storageService.deleteAll();
        return ResponseEntity.<FileStorage>ok().setData(null).setMessage("Images have been delete successfully ");
    }

}
