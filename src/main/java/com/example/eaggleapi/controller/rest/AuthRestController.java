package com.example.eaggleapi.controller.rest;

import com.example.eaggleapi.model.auth.ERole;
import com.example.eaggleapi.model.auth.Role;
import com.example.eaggleapi.model.auth.User;
import com.example.eaggleapi.payload.ResponseEntity;
import com.example.eaggleapi.payload.dto.JwtResponse;
import com.example.eaggleapi.payload.dto.RegisterResponse;
import com.example.eaggleapi.payload.mapper.AuthMapper;
import com.example.eaggleapi.payload.request.LoginRequest;
import com.example.eaggleapi.payload.request.RegisterRequest;
import com.example.eaggleapi.repository.RoleRepository;
import com.example.eaggleapi.repository.UserRepository;
import com.example.eaggleapi.security.UserDetailImp;
import com.example.eaggleapi.security.jwt.Jwtutils;
import com.example.eaggleapi.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

//      mark this class to Rest cause we implement API
@RestController
//      mark this for define Mapping level
@RequestMapping("/api/auth")
public class AuthRestController {
    //      inject class that we need via Autowired
    @Autowired
    private Jwtutils jwtutils;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    AuthService authService;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    UserRepository userRepository;
    @Autowired
    AuthMapper authMapper;

    //    mark this for use direct with post method
    @PostMapping("/login")
    public ResponseEntity<JwtResponse> loginUser(@RequestBody LoginRequest loginRequest) {
//      declare authentication interface for authenticate credential user
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getPhoneNumber(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtutils.generateToken(authentication);
        UserDetailImp userDetails = (UserDetailImp) authentication.getPrincipal();
//       get authorize role
        Set<String> roles = userDetails.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toSet());
        return ResponseEntity.<JwtResponse>ok().setData(
                new JwtResponse(userDetails.getId(), jwt, userDetails.getPhone_number(), LocalDate.now(), roles)
        ).setMessage("User have been login successfully");
    }

    @PostMapping("/register")
    public ResponseEntity<RegisterResponse> register(@RequestBody RegisterRequest registerRequest) {
        try {
//            validate phoneNumber when have duplicate
            int count = 0;
            List<User> validatePhoneNumber = authService.getPhoneNumber();
            for (User tmp : validatePhoneNumber) {
                if (tmp.getPhoneNumber().equals(registerRequest.getPhoneNumber())) {
                    count++;
                }
            }
//            instantiate object with assign value via parameter
            User user = new User(
                    registerRequest.getPhoneNumber(),
                    passwordEncoder.encode(registerRequest.getPassword())
            ).setName(registerRequest.getName());
            user.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
//            define role when register

            Set<String> strRoles = registerRequest.getRoles();
            Set<Role> roles = new HashSet<>();
            if (strRoles == null) {
                Role userRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                System.out.println(userRole);
                roles.add(userRole);
            } else {
                strRoles.forEach(role -> {
                    if ("admin".equals(role)) {
                        Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role Admin is not found."));
                        roles.add(adminRole);
                    }
                });
            }
            if (count != 0) {
                return ResponseEntity.<RegisterResponse>badRequest().setMessage("Your PhoneNumber is already exist !");
            } else {
                user.setRoles(roles);
                System.out.println(user);
                userRepository.save(user);
                RegisterResponse response = authMapper.userToRegisterResponse(user);
                response.setCreateDate(LocalDate.now());
                return ResponseEntity.<RegisterResponse>ok().setData(response).setMessage("User have been register successfully");
            }
        } catch (Exception e) {
            return ResponseEntity
                    .<RegisterResponse>wrongCredentials()
                    .setMessage("Invalid on user information")
                    .setSuccess(false);
        }

    }
}



