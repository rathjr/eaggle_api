package com.example.eaggleapi.controller.rest;

import com.example.eaggleapi.model.Contact;
import com.example.eaggleapi.model.Match;
import com.example.eaggleapi.payload.ResponseEntity;
import com.example.eaggleapi.payload.request.ContactRequest;
import com.example.eaggleapi.payload.request.MatchRequest;
import com.example.eaggleapi.service.ContactService;
import com.example.eaggleapi.service.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/contact")
public class ContactRestController {
    @Autowired
    ContactService contactService;

    @GetMapping
    public ResponseEntity<List<Contact>> findAllContact() {
        try {
//            declare list for store all post then return result
            List<Contact> ct = contactService.getAllContact();
            return ResponseEntity
                    .<List<Contact>>ok()
                    .setData(ct).setMessage("Contact have been found successfully");
        } catch (Exception ex) {
            return ResponseEntity
                    .<List<Contact>>exception()
                    .setMessage(ex.getMessage());
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Contact> createPost(@RequestBody ContactRequest contactRequest) {
        Contact contact = new Contact();
        contact.setName(contactRequest.getName());
        contact.setEmail(contactRequest.getEmail());
        contact.setPhoneNumber(contactRequest.getPhoneNumber());
//        declare boolean for call method create if true post well insert success
        boolean isCreate = contactService.create(contact);
        if (isCreate)
            return ResponseEntity.<Contact>ok().setData(contact).setMessage("Contact have been create successfully");
        else
            return ResponseEntity.<Contact>badRequest().setMessage("Check your Data again");
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Contact> delete(@PathVariable Long id) {
//       find post by ID first before delete
        Contact contact = contactService.findContactByID(id);
        if (contact== null) {
            return ResponseEntity
                    .<Contact>badRequest()
                    .setMessage("Your ID " + id + " is Not Found !");
        } else {
//            if found then it will delete
            boolean isDel = contactService.deleteContact(id);
            if (isDel) {
                return ResponseEntity.<Contact>ok().setData(contact).setMessage("Contact ID" + id + " have been delete successfully");
            } else
                return ResponseEntity.<Contact>badRequest().setMessage("Check your Data again");
        }
    }
}
