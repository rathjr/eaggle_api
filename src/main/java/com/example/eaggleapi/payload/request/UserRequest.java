package com.example.eaggleapi.payload.request;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserRequest {
    private String name;
    private String phoneNumber;
    private String profile;
}
