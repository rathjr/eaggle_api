package com.example.eaggleapi.payload.request;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;

@Setter
@Getter
public class MatchRequest {
    private String logo;
    private String result;
    private String name;
    private String stadium;
    private String dateOfMatch;
    private String ename;
    private String elogo;
    private String time;
    private String type;
    private String lresult;
}
