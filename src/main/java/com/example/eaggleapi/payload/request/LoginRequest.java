package com.example.eaggleapi.payload.request;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true)
public class LoginRequest {
    private String phoneNumber;
    private String password;
}
