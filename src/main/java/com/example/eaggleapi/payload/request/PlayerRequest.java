package com.example.eaggleapi.payload.request;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class PlayerRequest {
    private String name;
    private int number;
    private String position;
    private String placeOfBirth;
    private String height;
    private String weight;
    private String jointed;
    private String bestFoot;
    private String images;
    private String size;
}
