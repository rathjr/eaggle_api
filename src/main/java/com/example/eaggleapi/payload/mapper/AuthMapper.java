package com.example.eaggleapi.payload.mapper;

import com.example.eaggleapi.model.auth.User;
import com.example.eaggleapi.payload.dto.RegisterResponse;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AuthMapper {
    RegisterResponse userToRegisterResponse(User user);
    User registerResponseToUser(RegisterResponse response);
}
