package com.example.eaggleapi.payload.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@ToString
@AllArgsConstructor
@Accessors(chain = true)
public class JwtResponse {
    private Long id;
    private String token;
    private String phoneNumber;
    private LocalDate createDate=LocalDate.now();
    private Set<String> roles;
}
