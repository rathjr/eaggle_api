package com.example.eaggleapi.service.Imp;

import com.example.eaggleapi.model.Match;
import com.example.eaggleapi.repository.MatchRepository;
import com.example.eaggleapi.service.MatchService;
import com.example.eaggleapi.utils.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MatchImp implements MatchService {
    @Autowired
    MatchRepository matchRepository;
    @Override
    public List<Match> getAllMatchs() {
        return matchRepository.getAllMatchs();
    }

    @Override
    public boolean create(Match match) {
        return matchRepository.create(match);
    }

    @Override
    public Match findMatchByID(Long id) {
        return matchRepository.findMatchByID(id);
    }

    @Override
    public boolean deleteMatch(Long id) {
        return matchRepository.deleteMatch(id);
    }

    @Override
    public boolean updateMatch(Match match) {
        return matchRepository.updateMatch(match);
    }

    @Override
    public List<Match> findByPaging(Paging paging) {
        return matchRepository.findByPaging(paging);
    }

    @Override
    public int countAllMatch() {
        return matchRepository.countAllMatch();
    }
}
