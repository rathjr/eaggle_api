package com.example.eaggleapi.service.Imp;

import com.example.eaggleapi.model.Contact;
import com.example.eaggleapi.repository.ContactRepository;
import com.example.eaggleapi.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContactImp implements ContactService {
    @Autowired
    ContactRepository contactRepository;
    @Override
    public List<Contact> getAllContact() {
        return contactRepository.getAllContact();
    }

    @Override
    public boolean create(Contact contact) {
        return contactRepository.create(contact);
    }

    @Override
    public boolean deleteContact(Long id) {
        return contactRepository.deleteContact(id);
    }

    @Override
    public Contact findContactByID(Long id) {
        return contactRepository.findContactByID(id);
    }
}
