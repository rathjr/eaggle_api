package com.example.eaggleapi.service.Imp;

import com.example.eaggleapi.model.Player;
import com.example.eaggleapi.repository.PlayerRepository;
import com.example.eaggleapi.service.PlayerService;
import com.example.eaggleapi.utils.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayerImp implements PlayerService {
    @Autowired
    PlayerRepository playerRepository;

    @Override
    public List<Player> getAllPlayers() {
        return playerRepository.getAllPlayers();
    }

    @Override
    public boolean create(Player player) {
        return playerRepository.create(player);
    }

    @Override
    public Player findPlayerByID(Long id) {
        return playerRepository.findPlayerByID(id);
    }

    @Override
    public boolean deletePlayer(Long id) {
        return playerRepository.deletePlayer(id);
    }

    @Override
    public boolean updatePlayer(Player player) {
        return playerRepository.updatePlayer(player);
    }

    @Override
    public List<Player> findAllPlayerbyPosition(String ps) {
        return playerRepository.findAllPlayerbyPosition(ps);
    }

    @Override
    public List<Player> findByPaging(Paging paging) {
        return playerRepository.findByPaging(paging);
    }

    @Override
    public int countAllReport() {
        return playerRepository.countAllReport();
    }
}
