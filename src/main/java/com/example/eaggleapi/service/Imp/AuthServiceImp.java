package com.example.eaggleapi.service.Imp;


import com.example.eaggleapi.model.auth.User;
import com.example.eaggleapi.repository.UserRepository;
import com.example.eaggleapi.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Component
public class AuthServiceImp implements AuthService {
@Autowired
UserRepository userRepository;

    @Override
    public boolean registerUser(User user) {
        return userRepository.registerUser(user);
    }

    @Override
    public List<User> getPhoneNumber() {
        return userRepository.getPhoneNumber();
    }


}
