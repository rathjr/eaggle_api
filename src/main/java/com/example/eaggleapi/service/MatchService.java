package com.example.eaggleapi.service;

import com.example.eaggleapi.model.Match;
import com.example.eaggleapi.utils.Paging;

import java.util.List;

public interface MatchService {
    List<Match> getAllMatchs();
    boolean create(Match match);
    Match findMatchByID(Long id);
    boolean deleteMatch(Long id);
    boolean updateMatch(Match match);
    List<Match> findByPaging(Paging paging);
    int countAllMatch();
}
