package com.example.eaggleapi.service;


import com.example.eaggleapi.model.auth.User;

import java.util.List;

public interface AuthService {
    boolean registerUser(User user);
    List<User> getPhoneNumber();

}
