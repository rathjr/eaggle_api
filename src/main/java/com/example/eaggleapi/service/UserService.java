package com.example.eaggleapi.service;

import com.example.eaggleapi.model.auth.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserService {
    List<User> findAllUser();
    boolean updateUser(User user);
    User findUserByID(Long id);
    boolean deleteUsers(Long id);
}
