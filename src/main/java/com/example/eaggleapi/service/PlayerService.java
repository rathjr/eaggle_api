package com.example.eaggleapi.service;

import com.example.eaggleapi.model.Player;
import com.example.eaggleapi.utils.Paging;

import java.util.List;

public interface PlayerService {
    List<Player> getAllPlayers();
    boolean create(Player player);
    Player findPlayerByID(Long id);
    boolean deletePlayer(Long id);
    boolean updatePlayer(Player player);
    List<Player> findAllPlayerbyPosition(String ps);
    List<Player> findByPaging(Paging paging);
    int countAllReport();
}
