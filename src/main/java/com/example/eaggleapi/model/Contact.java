package com.example.eaggleapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
public class Contact {
    private Long id;
    private String email;
    private String name;
    private String phoneNumber;
    private LocalDate createDate=LocalDate.now();
}
