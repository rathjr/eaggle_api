package com.example.eaggleapi.model.auth;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@Accessors(chain = true)
public class User {
    private Long id;
    private String name;
    private String password;
    private String phoneNumber;
    private String profile;
    private LocalDate createDate=LocalDate.now();
    private Set<Role> roles = new HashSet<>();

    public User(String phoneNumber, String password) {
        this.phoneNumber = phoneNumber;
        this.password = password;
    }
    public User(){}
}
