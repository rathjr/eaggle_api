package com.example.eaggleapi.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FileStorage {
    private String url;
}
