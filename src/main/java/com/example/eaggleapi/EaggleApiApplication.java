package com.example.eaggleapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EaggleApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(EaggleApiApplication.class, args);
    }

}
