#!/bin/bash
# --------- prepare script to deploy spring ------------
mvn package
echo "--> project build successfully"
cd target
mv eaggle-api-0.0.1-SNAPSHOT.jar eaggle-api.jar
printf 'FROM adoptopenjdk/openjdk11\nRUN mkdir -p workspace\nCOPY /target/eaggle-api.jar /workspace\nEXPOSE 8080\nENTRYPOINT ["java","-jar","/workspace/eaggle-api.jar"]'>Dockerfile
echo "--> Dockerfile create successfully"
docker build -t apiimages .
echo "Images build successfully"
docker run -d -p 17000:8080 apiimages
echo "Deployment spring successfully"
docker ps
